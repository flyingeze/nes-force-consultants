<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Anada - Data Science & Analytics Template">

    <!-- ========== Page Title ========== -->
    <title>Anada - Data Science & Analytics Template</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">

    <!-- ========== Start Stylesheet ========== -->
    <link href="{{  asset('/') }}assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/themify-icons.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/flaticon-set.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/magnific-popup.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/owl.theme.default.min.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/animate.css" rel="stylesheet" />
    <link href="{{  asset('/') }}assets/css/bootsnav.css" rel="stylesheet" />
    <link href="{{  asset('/') }}style.css" rel="stylesheet">
    <link href="{{  asset('/') }}assets/css/responsive.css" rel="stylesheet" />
    <!-- ========== End Stylesheet ========== -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="{{  asset('/') }}assets/js/html5/html5shiv.min.js"></script>
      <script src="{{  asset('/') }}assets/js/html5/respond.min.js"></script>
    <![endif]-->

    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&amp;display=swap" rel="stylesheet">

</head>

<body>

<!-- Preloader Start -->
<div class="se-pre-con"></div>
<!-- Preloader Ends -->

<!-- Header -->
<header id="home">

    <!-- Start Navigation -->
    <nav class="navbar navbar-default transparent navbar-fixed navbar-transparent white bootsnav">

        <!-- Start Top Search -->
        <div class="container">
            <div class="row">
                <div class="top-search">
                    <div class="input-group">
                        <form action="#">
                            <input type="text" name="text" class="form-control" placeholder="Search">
                            <button type="submit">
                                <i class="ti-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container-full">

            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li class="search"><a href="#"><i class="ti-search"></i></a></li>
                    <li class="side-menu"><a href="#"><i class="ti-menu-alt"></i></a></li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{  url('/') }}">
                    <img src="{{  asset('/') }}assets/img/logo-light.png" class="logo logo-display" alt="Logo">
                    <img src="{{  asset('/') }}assets/img/logo.png" class="logo logo-scrolled" alt="Logo">
                </a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li>
                        <a href="{{  url('/') }}" class="active">Home</a>
                    </li>
                    <li>
                        <a href="{{  url('about-us') }}">About Company</a>
                    </li>
                    <li>
                        <a href="{{ url('services') }}">Services</a>
                    </li>
                    <li>
                        <a href="{{ url('blog') }}">Blog</a>
                    </li>
                    <li>
                        <a href="{{ url('contact') }}">Contact</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->

        </div>


        <!-- Start Side Menu -->
        <div class="side">
            <a href="#" class="close-side"><i class="ti-close"></i></a>
            <div class="widget">
                <h4 class="title">Contact Info</h4>
                <ul class="contact">
                    <li>
                        <div class="icon">
                            <i class="flaticon-email"></i>
                        </div>
                        <div class="info">
                            <span>Email</span> Info@gmail.com
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="flaticon-call-1"></i>
                        </div>
                        <div class="info">
                            <span>Phone</span> +123 456 7890
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="flaticon-countdown"></i>
                        </div>
                        <div class="info">
                            <span>Office Hours</span> Sat - Wed : 8:00 - 4:00
                        </div>
                    </li>
                </ul>
            </div>
            <div class="widget">
                <h4 class="title">Additional Links</h4>
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="widget social">
                <h4 class="title">Connect With Us</h4>
                <ul class="link">
                    <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="pinterest"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                    <li class="dribbble"><a href="#"><i class="fab fa-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- End Side Menu -->

    </nav>
    <!-- End Navigation -->

</header>
<!-- End Header -->

@yield('content')

<!-- Start Footer -->
<footer>
    <div class="container">
        <div class="f-items default-padding">
            <div class="row">
                <div class="equal-height col-lg-4 col-md-6 item">
                    <div class="f-item about">
                        <img src="{{  asset('/') }}assets/img/logo.png" alt="Logo">
                        <p>
                            Required honoured trifling eat pleasure man relation. Assurance yet bed was improving furniture man. Distrusts delighted
                        </p>
                    </div>
                </div>

                <div class="equal-height col-lg-2 col-md-6 item">
                    <div class="f-item link">
                        <h4 class="widget-title">Usefull Links</h4>
                        <ul>
                            <li>
                                <a href="#">Classic Business</a>
                            </li>
                            <li>
                                <a href="#">Blog</a>
                            </li>
                            <li>
                                <a href="#">Project</a>
                            </li>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="equal-height col-lg-2 col-md-6 item">
                    <div class="f-item link">
                        <h4 class="widget-title">Services</h4>
                        <ul>
                            <li>
                                <a href="#">Marketing & Sales</a>
                            </li>
                            <li>
                                <a href="#">Manufacturing</a>
                            </li>
                            <li>
                                <a href="#">Supply Chain</a>
                            </li>
                            <li>
                                <a href="#">Data Visualization</a>
                            </li>
                            <li>
                                <a href="#">Big Data</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="equal-height col-lg-4 col-md-6 item">
                    <div class="f-item contact">
                        <h4 class="widget-title">Contact Info</h4>
                        <p>
                            Possible offering at contempt mr distance stronger an. Attachment excellence announcing
                        </p>
                        <div class="address">
                            <ul>
                                <li>
                                    <strong>Email:</strong> support@validtheme.com
                                </li>
                                <li>
                                    <strong>Contact:</strong> +44-20-7328-4499
                                </li>
                            </ul>
                        </div>
                        <ul class="social">
                            <li class="facebook">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="twitter">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="youtube">
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </li>
                            <li class="instagram">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-lg-6">
                    <p>&copy; Copyright {{date('Y')}}. All Rights Reserved by <a href="#">validthemes</a></p>
                </div>
                <div class="col-lg-6 text-right link">
                    <ul>
                        <li>
                            <a href="#">Terms</a>
                        </li>
                        <li>
                            <a href="#">Privacy</a>
                        </li>
                        <li>
                            <a href="#">Support</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Shape -->
    <div class="footer-shape" style="background-image: url({{  asset('/') }}'assets/img/shape/1.svg');"></div>
    <!-- End Shape -->
</footer>
<!-- End Footer-->

<!-- jQuery Frameworks
============================================= -->
<script src="{{  asset('/') }}assets/js/jquery-1.12.4.min.js"></script>
<script src="{{  asset('/') }}assets/js/popper.min.js"></script>
<script src="{{  asset('/') }}assets/js/bootstrap.min.js"></script>
<script src="{{  asset('/') }}assets/js/equal-height.min.js"></script>
<script src="{{  asset('/') }}assets/js/jquery.appear.js"></script>
<script src="{{  asset('/') }}assets/js/jquery.easing.min.js"></script>
<script src="{{  asset('/') }}assets/js/jquery.magnific-popup.min.js"></script>
<script src="{{  asset('/') }}assets/js/modernizr.custom.13711.js"></script>
<script src="{{  asset('/') }}assets/js/owl.carousel.min.js"></script>
<script src="{{  asset('/') }}assets/js/wow.min.js"></script>
<script src="{{  asset('/') }}assets/js/progress-bar.min.js"></script>
<script src="{{  asset('/') }}assets/js/isotope.pkgd.min.js"></script>
<script src="{{  asset('/') }}assets/js/imagesloaded.pkgd.min.js"></script>
<script src="{{  asset('/') }}assets/js/count-to.js"></script>
<script src="{{  asset('/') }}assets/js/YTPlayer.min.js"></script>
<script src="{{  asset('/') }}assets/js/circle-progress.js"></script>
<script src="{{  asset('/') }}assets/js/bootsnav.js"></script>
<script src="{{  asset('/') }}assets/js/main.js"></script>

</body>
</html>
