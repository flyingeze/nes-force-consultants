@extends('master')

@section('content')
    <!-- Start Banner -->
    <div class="banner-area text-combo multi-heading bottom-shape top-pad-90 top-pad-35-mobile gradient-bg text-light">
        <div class="item">
            <div class="box-table">
                <div class="box-cell">
                    <div class="container">
                        <div class="row">
                            <div class="double-items">
                                <div class="col-lg-7 info">
                                    <h2 class="wow fadeInDown" data-wow-duration="1s">Applying Data Science  & Techniques <span>with Modern Systems</span></h2>
                                    <p class="wow fadeInLeft" data-wow-duration="1.5s">
                                        Contented continued any happiness instantly objection yet her allowance. Use correct day new brought tedious. Kept easy or sons my it done.
                                    </p>
                                    <a class="btn circle btn-md btn-transparent border inc-icon wow fadeInUp" data-wow-duration="1.8s" href="#">Get Started <i class="fas fa-angle-right"></i></a>
                                </div>
                                <div class="col-lg-5 thumb wow fadeInDown" data-wow-duration="1s">
                                    <img src="assets/img/illustration/5.png" alt="Thumb">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wavesshape">
                        <img src="assets/img/shape/4.svg" alt="Shape">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->

    <!-- Start About -->
    <div class="about-area text-center carousel-shadow wavesshape-bottom default-padding-top">
        <div class="container">
            <div class="about-items">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="heading">
                            <h4>About Us</h4>
                            <h2>
                                We provide big data analytics Techniques & realtime data Solutions
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="content">
                            <p>
                                Whole front do of plate heard oh ought. His defective nor convinced residence own. Connection has put impossible own apartments boisterous. At jointure ladyship an insisted so humanity he. Friendly bachelor entrance to on by. Extremity as if breakfast agreement. Off now mistress provided out horrible opinions. Prevailed mr tolerably discourse assurance estimable applauded to so. Him everything melancholy uncommonly but solicitude inhabiting projection.
                            </p>
                        </div>
                    </div>
                    <div class="feature-box text-left col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="feature-2-col-carousel owl-carousel owl-theme">
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Data Science</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-operative-system"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Machine Learning</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-chip"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Artificial Intelligence</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-robot"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="waveshape">
            <img src="assets/img/shape/2.svg" alt="Shape">
        </div>
    </div>
    <!-- End About -->

    <!-- Star Faq -->
    <div class="faq-area overflow-hidden rectangular-shape default-padding bg-gray">
        <div class="container">
            <div class="faq-items">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="faq-content">
                            <h2>We are master in <strong>Data science</strong> <br> And <strong>big data</strong> analysis</h2>
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h4 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Where can I get analytics help?
                                        </h4>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui consectetur at, sunt maxime, quod alias ullam officiis, ex necessitatibus similique odio aut! Provident, adipisci esse vero magni necessitatibus quisquam commodi.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            How much does data analytics costs?
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, illum earum nobis dolorum aliquid! Quos pariatur ipsam eum voluptates. Illum provident consequatur non aut labore, voluptates repudiandae maxime cum dolorem.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            What kind of data is needed for analysis?
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum iure accusamus ea, reprehenderit aspernatur deleniti corporis ad perspiciatis. Magnam sit enim animi, esse deleniti nobis quaerat veniam suscipit odit officiis.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="thumb wow fadeInLeft" data-wow-delay="0.5s">
                            <img src="assets/img/illustration/9.png" alt="Thumb">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Faq -->

    <!-- Star Companies -->
    <div class="companies-area bg-gray default-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Companies</h4>
                        <h2>
                            Trusted by great brands
                        </h2>
                    </div>
                </div>
            </div>
            <div class="companies-items">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="companies-carousel owl-carousel owl-theme">
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/1.png" alt="Thumb"></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/2.png" alt="Thumb"></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/3.png" alt="Thumb"></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/4.png" alt="Thumb"></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/5.png" alt="Thumb"></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="assets/img/clients/6.png" alt="Thumb"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Companies -->

    <!-- Start Chose Us Area -->
    <div class="choseus-area default-padding-top">
        <div class="container">
            <div class="choseus-items">
                <div class="row align-center">
                    <div class="col-lg-6">
                        <div class="thumb wow fadeInLeft" data-wow-delay="0.5s">
                            <img src="assets/img/illustration/8.png" alt="Thumb">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="info wow fadeInUp">
                            <h2>Transform Your Business Faster Using Data Science and Analysis</h2>
                            <p>
                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                            </p>
                            <h4>3 Easy Steps to Get Started</h4>
                            <ul>
                                <li>
                                    <span><i class="ti-check"></i> Understand Databases</span>
                                </li>
                                <li>
                                    <span><i class="ti-check"></i> Explore The Data Science Workflow</span>
                                </li>
                                <li>
                                    <span><i class="ti-check"></i> Level Up with Big Data</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Chose Us -->

    <!-- Start Projects Area -->
    <div class="projects-area overflow-hidden carousel-shadow default-padding-top">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/2.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="heading-left">
                <div class="row">
                    <div class="col-lg-5">
                        <h2>
                            Let's see our Recent & popular completed projects
                        </h2>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <p>
                            Everything melancholy uncommonly but solicitude inhabiting projection off. Connection stimulated estimating excellence an to impression.
                        </p>
                        <a class="btn circle btn-md btn-gradient wow fadeInUp" href="#">View All <i class="fas fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="projects-items col-lg-12 wow fadeInUp" data-wow-delay="0.2s">
                <div class="row">
                    <div class="projects-carousel owl-carousel owl-theme">
                        <!-- Single Item -->
                        <div class="single-item">
                            <div class="thumb item-effect">
                                <img src="assets/img/case/1.jpg" alt="Thumb">
                                <div class="effect-info">
                                    <a href="assets/img/case/1.jpg" class="item popup-gallery">
                                        <i class="fas fa-camera"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h5>
                                    <a href="#">Analysis Competitors</a>
                                </h5>
                                <ul>
                                    <li>Data</li>
                                    <li>Clicks</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <div class="single-item">
                            <div class="thumb item-effect">
                                <img src="assets/img/case/2.jpg" alt="Thumb">
                                <div class="effect-info">
                                    <a href="https://www.youtube.com/watch?v=owhuBrGIOsE" class="item popup-youtube">
                                        <i class="fas fa-video"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h5>
                                    <a href="#">Workload Automation</a>
                                </h5>
                                <ul>
                                    <li>Testing</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <div class="single-item">
                            <div class="thumb item-effect">
                                <img src="assets/img/case/3.jpg" alt="Thumb">
                                <div class="effect-info">
                                    <a href="assets/img/case/3.jpg" class="item popup-gallery">
                                        <i class="fas fa-camera"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h5>
                                    <a href="#">File Transfers</a>
                                </h5>
                                <ul>
                                    <li>Monitor</li>
                                    <li>Science</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <div class="single-item">
                            <div class="thumb item-effect">
                                <img src="assets/img/case/4.jpg" alt="Thumb">
                                <div class="effect-info">
                                    <a href="https://www.youtube.com/watch?v=owhuBrGIOsE" class="item popup-youtube">
                                        <i class="fas fa-video"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h5>
                                    <a href="#">ChatBots Creation</a>
                                </h5>
                                <ul>
                                    <li>Algorithm</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Projects Area -->

    <!-- Start Fun Factor Area -->
    <div class="fun-factor-area default-padding">

        <div class="container">
            <div class="client-items text-center">
                <!-- Fixed BG -->
                <div class="fixed-bg contain" style="background-image: url(assets/img/map.svg);"></div>
                <!-- Fixed BG -->
                <div class="row">
                    <div class="col-lg-3 col-md-6 item">
                        <div class="fun-fact">
                            <div class="timer" data-to="687" data-speed="5000">687</div>
                            <span class="medium">Projects Executed</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 item">
                        <div class="fun-fact">
                            <div class="timer" data-to="2348" data-speed="5000">2348</div>
                            <span class="medium">Data Analytics</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 item">
                        <div class="fun-fact">
                            <div class="timer" data-to="450" data-speed="5000">450</div>
                            <span class="medium">Data Management</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 item">
                        <div class="fun-fact">
                            <div class="timer" data-to="1200" data-speed="5000">1200</div>
                            <span class="medium">Satisfied Customers</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Fun Factor Area -->

    <!-- Start Pricing Area -->
    <div class="pricing-area overflow-hidden rectangular-shape shape-margin-right default-padding-bottom bottom-less">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Our Pricing</h4>
                        <h2>
                            Select Your Choice
                        </h2>
                    </div>
                </div>
            </div>
            <div class="pricing-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="pricing-item wow fadeInUp" data-wow-delay="400ms">
                            <ul>
                                <li class="pricing-header">
                                    <div class="icon">
                                        <div class="shape-top">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                        <i class="flaticon-start"></i>
                                        <div class="shape-bottom">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                    </div>
                                    <h5>Trial Version</h5>
                                    <h2><sup>$</sup>0.00</h2>
                                </li>
                                <li><i class="ti-close"></i> Demo file</li>
                                <li><i class="ti-check"></i> Update</li>
                                <li><i class="ti-check"></i> Commercial use</li>
                                <li><i class="ti-close"></i> Support</li>
                                <li><i class="ti-check"></i> 2 database</li>
                                <li class="footer">
                                    <a class="btn circle btn-sm btn-gray effect" href="#">Try for free</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single Item -->

                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="pricing-item active wow fadeInUp" data-wow-delay="500ms">
                            <ul>
                                <li class="pricing-header">
                                    <div class="icon">
                                        <div class="shape-top">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                        <i class="flaticon-medal-1"></i>
                                        <div class="shape-bottom">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                    </div>
                                    <h5>Regular</h5>
                                    <h2><sup>$</sup>29 <sub>/ M</sub></h2>
                                </li>
                                <li><i class="ti-close"></i> Demo file</li>
                                <li><i class="ti-check"></i> Update</li>
                                <li><i class="ti-close"></i> Commercial use</li>
                                <li><i class="ti-check"></i> Support</li>
                                <li><i class="ti-check"></i> 8 database</li>
                                <li class="footer">
                                    <a class="btn circle btn-sm btn-gradient" href="#">Get Started</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single Item -->

                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="pricing-item wow fadeInUp" data-wow-delay="600ms">
                            <ul>
                                <li class="pricing-header">
                                    <div class="icon">
                                        <div class="shape-top">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                        <i class="flaticon-value"></i>
                                        <div class="shape-bottom">
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                            <div class="shape"></div>
                                        </div>
                                    </div>
                                    <h5>Extended</h5>
                                    <h2><sup>$</sup>59 <sub>/ Y</sub></h2>
                                </li>
                                <li><i class="ti-check"></i> Demo file</li>
                                <li><i class="ti-check"></i> Update</li>
                                <li><i class="ti-close"></i> Commercial use</li>
                                <li><i class="ti-check"></i> Support</li>
                                <li><i class="ti-check"></i> 8 database</li>
                                <li class="footer">
                                    <a class="btn circle btn-sm btn-gray effect" href="#">Get Started</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Pricing Area -->

    <!-- Start Blog Area -->
    <div class="blog-area bg-gray default-padding bottom-less">
        <div class="container">
            <div class="heading-left">
                <div class="row">
                    <div class="col-lg-5">
                        <h2>
                            Stay Update with Anada from latest & popular News
                        </h2>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <p>
                            Everything melancholy uncommonly but solicitude inhabiting projection off. Connection stimulated estimating excellence an to impression.
                        </p>
                        <a class="btn circle btn-md btn-gradient wow fadeInUp" href="#">View All <i class="fas fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="blog-items">
                <div class="row">
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <a href="#">
                                    <img src="assets/img/blog/1.jpg" alt="Thumb">
                                    <div class="date">
                                        18 Jul <strong>2020</strong>
                                    </div>
                                </a>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-folder-open"></i>
                                        </li>
                                        <li>
                                            <a href="#">Process</a>
                                        </li>
                                        <li>
                                            <a href="#">Store</a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>
                                    <a href="#">Analysis rendered entir Highly indeed to garden</a>
                                </h4>
                                <p>
                                    Offered chiefly farther of my no colonel shyness. Such on help ye some door if in. Laughter proposal laughing.
                                </p>
                                <div class="footer-meta">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fas fa-user"></i> Author</a>
                                        </li>
                                        <li>
                                            <a class="btn-simple" href="#"><i class="fas fa-angle-right"></i> Read More</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <a href="#">
                                    <img src="assets/img/blog/2.jpg" alt="Thumb">
                                    <div class="date">
                                        05 Aug <strong>2020</strong>
                                    </div>
                                </a>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-folder-open"></i>
                                        </li>
                                        <li>
                                            <a href="#">Data</a>
                                        </li>
                                        <li>
                                            <a href="#">Analysis</a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>
                                    <a href="#">Lasted am so before on esteem vanity oh. </a>
                                </h4>
                                <p>
                                    Offered chiefly farther of my no colonel shyness. Such on help ye some door if in. Laughter proposal laughing.
                                </p>
                                <div class="footer-meta">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fas fa-user"></i> Author</a>
                                        </li>
                                        <li>
                                            <a class="btn-simple" href="#"><i class="fas fa-angle-right"></i> Read More</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <a href="#">
                                    <img src="assets/img/blog/3.jpg" alt="Thumb">
                                    <div class="date">
                                        27 Dec <strong>2020</strong>
                                    </div>
                                </a>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-folder-open"></i>
                                        </li>
                                        <li>
                                            <a href="#">Atrificial</a>
                                        </li>
                                        <li>
                                            <a href="#">Data</a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>
                                    <a href="#">Repeated of endeavor mr position kindness.</a>
                                </h4>
                                <p>
                                    Offered chiefly farther of my no colonel shyness. Such on help ye some door if in. Laughter proposal laughing.
                                </p>
                                <div class="footer-meta">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fas fa-user"></i> Author</a>
                                        </li>
                                        <li>
                                            <a class="btn-simple" href="#"><i class="fas fa-angle-right"></i> Read More</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Blog Area -->

    <!-- Start Start With Us Area -->
    <div class="start-us-area overflow-hidden bg-gradient text-light default-padding">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-7">
                    <div class="info wow fadeInLeft">
                        <h2>Get Ready to Start <br>Your Project With Us</h2>
                        <p>
                            Jointure ladyship an insisted so humanity he. Friendly bachelor entrance to on by. Extremity as if breakfast agreement. Off now mistress provided out horrible opinions. Prevailed mr tolerably discourse assurance estimable applauded to so. Him everything melancholy uncommonly but solicitude inhabiting projection.
                        </p>
                        <a class="btn-standard md" href="#"><i class="fas fa-angle-right"></i> Read More</a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="thumb wow fadeInUp" data-wow-delay="400ms">
                        <img src="assets/img/illustration/7.png" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Start With Us Area -->
@endsection
