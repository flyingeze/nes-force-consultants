@extends('master')

@section('content')

    <!-- Start Breadcrumb  -->
    <div class="breadcrumb-area gradient-bg text-light text-center">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>About Us</h1>
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Start Our About -->
    <div class="about-area bg-gray overflow-hidden rectangular-shape default-padding">
        <div class="container">
            <div class="about-items choseus-items right-thumb">
                <div class="row align-center">
                    <div class="col-lg-6">
                        <div class="info wow fadeInLeft">
                            <h2>We provide big data analytics Techniques & Data Solutions</h2>
                            <p>
                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                            </p>
                            <h4>3 Easy Steps to Get Started</h4>
                            <ul>
                                <li>
                                    <span><i class="ti-check"></i> Understand Databases</span>
                                </li>
                                <li>
                                    <span><i class="ti-check"></i> Explore The Data Science Workflow</span>
                                </li>
                                <li>
                                    <span><i class="ti-check"></i> Level Up with Big Data</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="thumb wow fadeInUp" data-wow-delay="0.5s">
                            <img src="assets/img/illustration/9.png" alt="Thumb">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Our About -->

    <!-- Star Features -->
    <div class="features-list bottom-less text-light bg-gray">
        <div class="container">
            <div class="row">
                <!-- Single Item -->
                <div class="single-item col-lg-4">
                    <div class="item wow fadeInUp" data-wow-delay="400ms">
                        <i class="flaticon-server-2"></i>
                        <h4>Data Science</h4>
                        <ul>
                            <li>The Data Analyst</li>
                            <li>The Data Engineer</li>
                            <li>Machine Learning</li>
                            <li>Data Generalist</li>
                        </ul>
                    </div>
                </div>
                <!-- Single Item -->
                <!-- Single Item -->
                <div class="single-item col-lg-4">
                    <div class="item wow fadeInUp" data-wow-delay="500ms">
                        <i class="flaticon-robot"></i>
                        <h4>Artificial Intelligence</h4>
                        <ul>
                            <li>Reactive Machines</li>
                            <li>Limited Memory</li>
                            <li>Theory of Mind</li>
                            <li>Artificial Narrow Intelligence</li>
                        </ul>
                    </div>
                </div>
                <!-- Single Item -->
                <!-- Single Item -->
                <div class="single-item col-lg-4">
                    <div class="item wow fadeInUp" data-wow-delay="600ms">
                        <i class="flaticon-machine-learning-1"></i>
                        <h4>Machine Learning</h4>
                        <ul>
                            <li>Linear Regression</li>
                            <li>Support Vector Machines</li>
                            <li>Random Forest</li>
                            <li>Ensemble Methods</li>
                        </ul>
                    </div>
                </div>
                <!-- Single Item -->
            </div>
        </div>
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/9.svg);"></div>
        <!-- Fixed BG -->
    </div>
    <!-- End Features -->

    <!-- Star Our Story -->
    <div class="our-story-area default-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Our Story</h4>
                        <h2>
                            How to get started
                        </h2>
                    </div>
                </div>
            </div>
            <div class="story-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="col-lg-3 col-md-6 single-item">
                        <div class="item">
                            <span>01</span>
                            <h4>Company started</h4>
                            <p>15/02/2015</p>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 col-md-6 single-item">
                        <div class="item">
                            <span>02</span>
                            <h4>Reached 2k+ Customers</h4>
                            <p>09/08/2016</p>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 col-md-6 single-item">
                        <div class="item">
                            <span>03</span>
                            <h4>Increase Workers</h4>
                            <p>26/08/2018</p>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 col-md-6 single-item">
                        <div class="item">
                            <span>04</span>
                            <h4>Win Multiple Award</h4>
                            <p>09/02/2020</p>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Our Story -->

    <!-- Start Skill Area -->
    <div class="skill-area overflow-hidden center-mobile circular-shape default-padding">
        <div class="container">
            <div class="skill-items">
                <div class="row">
                    <div class="col-lg-6 info">
                        <h2>Our Skill</h2>
                        <h3>We are master in data science <br> & big data analysis</h3>
                        <p>
                            Attention sex questions applauded how happiness. To travelling occasional at oh sympathize prosperous. His merit end means widow songs linen known. Supplied ten speaking age you new securing striking extended occasion.
                        </p>
                        <div class="skill-items">
                            <!-- Progress Bar Start -->
                            <div class="progress-box">
                                <h5>Data Anaylsis <span class="float-right">88%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" data-width="88"></div>
                                </div>
                            </div>
                            <div class="progress-box">
                                <h5>Social Assistant <span class="float-right">95%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" data-width="95"></div>
                                </div>
                            </div>
                            <div class="progress-box">
                                <h5>AI Solutions <span class="float-right">70%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" data-width="70"></div>
                                </div>
                            </div>
                            <!-- End Progressbar -->
                        </div>
                    </div>
                    <div class="col-lg-6 thumb">
                        <div class="image-box">
                            <img class="wow fadeInRight" data-wow-delay="0.6s" src="assets/img/app/3.jpg" alt="Thumb">
                            <img class="wow fadeInLeft" data-wow-delay="0.8s"  src="assets/img/app/1.jpg" alt="Thumb">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Skill Area -->

    <!-- Start Our Team -->
    <div class="our-team-area default-padding-bottom bottom-less">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Our Team</h4>
                        <h2>
                            Meet our experts
                        </h2>
                    </div>
                </div>
            </div>
            <div class="team-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="single-item col-lg-4 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="assets/img/team/1.jpg" alt="Thumb">
                            </div>
                            <div class="info">
                                <div class="social">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>Jonathom Dan</h4>
                                <span>Project Manager</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="single-item col-lg-4 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="assets/img/team/2.jpg" alt="Thumb">
                            </div>
                            <div class="info">
                                <div class="social">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>Munia Ankor</h4>
                                <span>Co-Founder</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="single-item col-lg-4 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="assets/img/team/3.jpg" alt="Thumb">
                            </div>
                            <div class="info">
                                <div class="social">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>Dunal lays</h4>
                                <span>Assistant manager</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Our Team -->
@endsection
