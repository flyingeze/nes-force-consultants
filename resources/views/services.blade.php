@extends('master')

@section('content')

    <!-- Start Breadcrumb
    ============================================= -->
    <div class="breadcrumb-area gradient-bg text-light text-center">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>Our Services</h1>
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">Services</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Start Services
    ============================================= -->
    <div class="services-area text-center carousel-shadow wavesshape-bottom default-padding">
        <div class="container">
            <div class="about-items">
                <div class="row">
                    <div class="feature-box text-left col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="feature-2-col-carousel owl-carousel owl-theme">
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Data Science</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-operative-system"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Machine Learning</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-chip"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                    <!-- Single Item -->
                                    <div class="item">
                                        <div class="info">
                                            <h4>Artificial Intelligence</h4>
                                            <p>
                                                Regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.
                                            </p>
                                        </div>
                                        <div class="bottom">
                                            <i class="flaticon-robot"></i>
                                            <a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- End Single Item -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Services -->

    <!-- Start Work Process Area
    ============================================= -->
    <div class="work-process-area default-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Work Process</h4>
                        <h2>
                            How We Work
                        </h2>
                    </div>
                </div>
            </div>
            <div class="works-process-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="col-lg-4 single-item">
                        <div class="item">
                            <div class="icon">
                                <i class="flaticon-problem"></i>
                                <span>01</span>
                            </div>
                            <div class="info">
                                <h4>Frame the Problem</h4>
                                <p>
                                    Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->

                    <!-- Single Item -->
                    <div class="col-lg-4 single-item">
                        <div class="item">
                            <div class="icon">
                                <i class="flaticon-data-collection"></i>
                                <span>02</span>
                            </div>
                            <div class="info">
                                <h4>Collect Data</h4>
                                <p>
                                    Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->

                    <!-- Single Item -->
                    <div class="col-lg-4 single-item">
                        <div class="item">
                            <div class="icon">
                                <i class="flaticon-cloud-data"></i>
                                <span>03</span>
                            </div>
                            <div class="info">
                                <h4>Process Data</h4>
                                <p>
                                    Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Work Process Area -->

    <!-- Start Start With Us Area
    ============================================= -->
    <div class="start-us-area overflow-hidden bg-gradient text-light default-padding">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-7">
                    <div class="info wow fadeInLeft">
                        <h2>Get Ready to Start <br>Your Project With Us</h2>
                        <p>
                            Jointure ladyship an insisted so humanity he. Friendly bachelor entrance to on by. Extremity as if breakfast agreement. Off now mistress provided out horrible opinions. Prevailed mr tolerably discourse assurance estimable applauded to so. Him everything melancholy uncommonly but solicitude inhabiting projection.
                        </p>
                        <a class="btn-standard md" href="#"><i class="fas fa-angle-right"></i> Read More</a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="thumb wow fadeInUp" data-wow-delay="400ms">
                        <img src="assets/img/illustration/7.png" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Start With Us Area -->
@endsection
